---
title: "Academia de videojuegos"
image: "images/portfolio/item-5.png"
project_url : "www.videojuegosba.com.ar"
categories: ["Contenidos", "Diseño Gráfico"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

Este proyecto del Gobierno de la Ciudad de Buenos Aires se centró en tres verticales relacionadas a esports y videojuegos: educación, competencias y charlas. Dirigiendo las relaciones públicas realizamos análisis y planificaciones que apuntaron al público objetivo y permitieron alcanzar exponentes como speakers de varias conferencias.
