---
title: "Goya"
image: "images/portfolio/item-7.png"
project_url : "https://www.goya.gob.ar/"
categories: ["Contenidos"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

Conformamos la identidad visual del proyecto teniendo en cuenta cuestiones de branding de la Ciudad de Goya (Corrientes), planificamos estratégicamente las acciones de comunicación para la difusión de apertura de inscripciones, continuidad y match days de las competencias, y establecimos mecanismos de comunicación interna con los participantes. Gestionamos la prensa especializada con periodistas de la zona y fuimos fuente de consulta para varios medios de comunicación.
