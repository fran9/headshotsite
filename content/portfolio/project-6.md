---
title: "TEKO 3.0"
image: "images/portfolio/item-6.png"
project_url : "www.instagram.com/teko3.0"
categories: ["Contenidos", "Diseño Gráfico", "Community management"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

Parametrizamos la sistematización de contenidos para redes sociales, planificamos estrategias comunicativas aplicadas a dar a conocer el proyecto y sus actividades. Trabajamos en paralelo la comunicación de 5 competencias de esports, que incluyen envío masivo de mails, diseños de overlays para stream y difusión general con destino a comunidades interesadas en participar de las competencias. Este proyecto es llevado a cabo por el Gobierno de la Provincia de Corrientes.

