---
title: "XFX Argentina"
image: "images/portfolio/item-8.png"
project_url : "www.xfx.com.ar"
categories: ["Web", "Contenidos", "Influencers mkt", "Branding"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

Una marca creada conceptual y visualmente por nosotros. Desde su concepto creativo y filosófico, hasta la ejecución de su identidad mediante el diseño de packagings, folletos, y contenidos publicitarios. Creamos su sitio web y realizamos campañas de posicionamiento orgánico a través de contenidos de blogs, y pautado por campañas correctamente segmentadas. En comunicación digital, llevamos adelante un look and feel en sus canales, y mediante acciones con influencers referentes hemos generado una comunidad específica de marca.