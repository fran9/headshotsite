---
title: "Wombo Academy"
image: "images/portfolio/item-4.png"
project_url : "www.wombo.gg"
categories: ["Prensa", "Contenidos"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

Gestionamos prensa especializada en deportes electrónicos, elaboramos los comunicados de prensa, el mapeo de medios de comunicación y periodistas afines a los objetivos de la marca. Monitoreamos el social listening de la marca en Internet, para esto utilizamos distintas herramientas que nos permiten saber lo que se está diciendo. Confeccionamos el clipping mensual con las menciones correspondientes.