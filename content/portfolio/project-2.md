---
title: "Liga Burger King + Pepsi"
image: "images/portfolio/item-2.png"
project_url : "www.ligabkpepsi.com.ar"
categories: ["Contenidos" , "Diseño Gráfico"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

 Contenidos especializados en gaming & esports, semanalmente redactamos informaciones de prensa relacionadas al mundo de los videojuegos y elaboramos piezas de comunicación para redes sociales. Somos partners de la vertical Gaming & Esports de Moscú Agencia, gracias a esto experimentamos profesionalmente en distintos proyectos de comunicación de gran calibre.
