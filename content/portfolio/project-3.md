---
title: "EFL (Esports Food League)"
image: "images/portfolio/item-3.png"
project_url : "https://linktr.ee/efoodleague"
categories: ["Contenidos", "Prensa", "Diseño Gráfico"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

La EFL es la primer liga gastronómica de Esports de 	Argentina, esta liga incluye a 8 marcas las cuales patrocinan a un equipo de LOL cada una, gracias a la vertical de Gaming & Esports de Agencia Moscú formamos parte de este gran proyecto. Nuestro trabajó se centra en entrevistar equipos y referentes de las marcas partes de la EFL, redactar informaciones de prensa y gestionar la prensa especializada. Ayudamos a las marcas a introducirse en los medios a través de estas acciones novedosas de entretenimiento. 
