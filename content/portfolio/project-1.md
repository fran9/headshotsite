---
title: "Torneo CS GO - Despegar"
image: "images/portfolio/item-1.png"
project_url : "www.despegar.com.ar"
categories: ["Contenidos","Comunicación Interna","Diseño Gráfico", "Marketing digital"]
description: "This is meta description."
draft: false
---

#### Datos del proyecto

 La acción de team building llevada a cabo por la empresa se centró en la realización de un torneo de CSGO para el equipo interno de la marca. Nuestra función fue analizar la identidad y filosofía de la marca para incorporar comunicaciones que respeten la cultura interna, y agregando el toque creativo que requieren este tipo de campañas. Realizamos comunicaciones internas hacia el personal respetando los canales de comunicación de la marca.