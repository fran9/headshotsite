---
title: "Nosotros"
description: "This is meta description."
author_image : "images/about/author.jpg"
author_signature : "images/about/signature.png"
draft: false
---

Llegamos para ser una de las primeras agencias de relaciones públicas dedicada a la comunicación en la industria de los videojuegos, nacimos desde la consultora Smok Media para también acompañar a la industria de los Esport con profesionales de disciplinas diversas de las ciencias de la comunicación.
Creamos mensajes claves y estratégicos para generar un impacto en el mundo de los videojuegos, además tenemos como eje la disciplina de las Relaciones Públicas para generar vínculos entre organizaciones de la industria gaming. Desde HEADSHOT nos apoyamos en nuestro amor y pasión por los videojuegos para transmitir esta esencia en el trabajo profesional; junto a esto debemos agregar que somos claros y precisos, apuntamos y disparamos comunicación de impacto.

Nuestro objetivo es evolucionar la forma de comunicar proyectos relacionados a la industria de los videojuegos y lo lograremos ya que nuestra visión es ser la primera elección de agencias, productoras, organizaciones o marcas que busquen difundir con impacto sus propuestas relacionadas al mundo del Gaming y los Esports. Todas estas metas nos convertirán en la primera agencia de relaciones públicas especializada en estos ámbitos con mayor posicionamiento en LATAM.

Poseemos amplia experiencia debido a los trabajos realizados con distintas marcas y organizaciones del mundo de los videojuegos (DEVA, Buenos Aires Esports, XFX Argentina, Wombo Academy, Moscú Gaming & Esports, entre otros). El disparador de este proyecto se debe gracias a la pasión que tenemos por el Gaming y los Esports, como también la profesionalidad que poseemos a la hora de crear mensajes claros y precisos.
Somos Headshot, agencia de relaciones públicas.
